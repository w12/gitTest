//
//  People.m
//  test
//
//  Created by mac on 16/8/3.
//  Copyright © 2016年 JaSon. All rights reserved.
//

#import "People.h"

@implementation People

-(id)init{
    self = [super init];
    if (self) {
        _car = [[Car alloc]init];
    }
    return self;
}

-(id)initWithDic:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

-(void)eat{
    NSLog(@"name ---- %@,age---------%d",self.name,self.age);
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

@end
