//
//  People.h
//  test
//
//  Created by mac on 16/8/3.
//  Copyright © 2016年 JaSon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"

@interface People : NSObject
{
    NSString *_nikName;
}
@property (nonatomic,assign) int age;

@property (nonatomic,copy) NSString *name;

@property (nonatomic,strong) Car *car;

-(void)eat;

-(id)initWithDic:(NSDictionary *)dic;

@end
