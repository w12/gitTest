//
//  ViewController.m
//  test
//
//  Created by mac on 16/8/3.
//  Copyright © 2016年 JaSon. All rights reserved.
//

#import "ViewController.h"
#import "People.h"

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor greenColor];
//    [self case2];
    [self case3];
}

-(void)case3 {
    NSDictionary *dic = @{@"name":@"你大爷",@"age":@"160"};
    People *p = [[People alloc]initWithDic:dic];
    NSLog(@"----- %@,-------%@",[p valueForKey:@"name"],[p valueForKey:@"age"]);
    
}

-(void)case2 {
    People *p = [[People alloc]init];
    [p setValue:@"奥迪" forKeyPath:@"car.name"];
    NSLog(@"car ---- %@",[p valueForKeyPath:@"car.name"]);
}

-(void)case1 {
    UIButton *button = [[UIButton alloc]init];
    button.frame = CGRectMake(0, 0, 100, 50);
    [button setTitle:@"按钮" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(aa) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    People *p = [[People alloc]init];
    p.name = @"张三";
    p.age = 18;
    NSLog(@"%@,%d",p.name,p.age);
    
    [p setValue:@"张三" forKey:@"name"];
    [p setValue:@"18" forKey:@"age"];
    NSLog(@"%@,%@",[p valueForKey:@"name"],[p valueForKey:@"age"]);
    
    [p setValue:@"怂逼" forKey:@"nikName"];
    NSLog(@"%@",[p valueForKey:@"nikName"]);
}

-(void)aa{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
