//
//  Car.h
//  test
//
//  Created by mac on 16/8/3.
//  Copyright © 2016年 JaSon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Car : NSObject

@property (nonatomic,copy) NSString *name;

@end
